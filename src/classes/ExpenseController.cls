public with sharing class ExpenseController {
	@AuraEnabled
    public static List<Expenses__c> getExpenses() 
    {
    // Perform isAccessible() check here
    	return [SELECT Id, Name, Amount__c, Client__c, Date__c, Reimbursed__c, CreatedDate FROM Expenses__c];
    }
    @AuraEnabled
	public static Expenses__c saveExpense(Expenses__c expense) 
	{
		// Perform isUpdateable() check here
		upsert expense;
		return expense;
	}
}