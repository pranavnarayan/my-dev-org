global class AngularTryOutPageController {
    
    global static Id AccountID{get;set;}

    @RemoteAction
    global static List<wrapperClass> dispContacts(String accId) {
     
     List<wrapperClass> wrapperList = new List<wrapperClass>();
     for(contact c: [Select id,Name,Firstname,LastName,email,phone from Contact where accountid = :accId])
     {
     	wrapperClass wrapperObj = new wrapperClass(false,c);
     	wrapperList.add(wrapperObj);
     }
     return wrapperList;
    
    }

    @RemoteAction
    global static String massDelContacts(String contactstobedel){
        String result='';
        List<Contact> tobeDeleted = (List<contact>)JSON.deserialize(contactstobedel, List<Contact>.class);
        try
        {
            delete tobeDeleted;
            result = 'Success';
        }
        catch(Exception e)
        {
            result = 'Exception:- ' + e.getDMLMessage(0);
            system.debug(''+e.getDMLMEssage(0));
        }
        return result;
    }





     @RemoteAction
    global static String delContact(String conId) {
     system.debug('conId' + conId);
     String result='';
     Contact deletecontact = new Contact(id=conid);
    // List<String> finalString = new List<String>();
     Boolean deleted = false;
     try{
     	delete deletecontact;
     	result = 'Success';
     }
     catch(Exception e)
     {
     		result = 'Exception:- '+e.getDMLMEssage(0);
     		system.debug(''+e.getDMLMEssage(0));
     }
     return result;
    
    }
    @RemoteAction
    global static String GetAccName(String accId) {
     
     return [Select name from Account where Id = :accId limit 1].Name ;
    
    }
    @RemoteAction
    global static String updateContact(String con)
    {
    	Contact tobeUpdated = (Contact)JSON.deserialize(con, Contact.class);
    	String result ='';
    	try
    	{
    		update tobeUpdated;
    		result = 'Success';
    	}
    	catch(exception e)
    	{
    		result = 'Exception:- '+e.getDMLMEssage(0);
    		//errorsThere = true;
    	}
    	return result;
    }


    @RemoteAction
    global static String insertContact(String con)
    {

    	system.debug('String'+con);
    	Contact tobeInserted = (Contact)JSON.deserialize(con, Contact.class);
        //system.debug('velaimatter'+AngularTryOutPageController.AccountID);
        //tobeInserted.AccountID = AngularTryOutPageController.AccountID;
    	String result ='';
    	try
    	{
    		insert tobeInserted;
    		result = 'Success';
    	}
    	catch(exception e)
    	{
    		result = 'Exception:- '+e.getDMLMEssage(0);
    		//errorsThere = true;
    	}
    	return result;
    }



    global class wrapperClass
    {
    	public Boolean isChecked {get;set;}
    	public Contact cont{get;set;}
    	wrapperClass(Boolean ischecked,Contact cont)
    	{
    		this.isChecked = ischecked;
    		this.cont = cont;
    	}
    }


}