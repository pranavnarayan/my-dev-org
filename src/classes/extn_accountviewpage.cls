public with sharing class extn_accountviewpage{
    private Account thisAccount {get;set;}
    private Id thisAccountId{get;set;}
    public extn_accountviewpage(ApexPages.StandardController controller) {
        thisAccount = (Account)controller.getRecord();
        thisAccountId = controller.getId();
    }
    public Account getAccount(){
        return thisAccount;
    }
    public Id getAccountID(){
        return thisAccountId;
    }

}