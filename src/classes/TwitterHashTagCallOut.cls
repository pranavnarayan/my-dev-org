public with sharing class TwitterHashTagCallOut {
	public String responseBody {get;set;}
	public String responseBodyforSearch{get;set;}
	public loginResponse lresponse {Get;set;}

	public List<wrappedResponse> allTexts {get;set;}
	public Map<String,String> responseforSearch{get;set;}
	public TwitterHashTagCallOut(){
		allTexts = new List<wrappedResponse>();
	}
	public class wrappedResponse{
		public String link{get;set;}
		public String Userlink{get;set;}
		public String UserName {get;set;}
		public String Location{get;set;}


	}

	public class loginResponse{
		public string token_type{get;set;}
		public string access_token{get;set;}
	}


	public static HttpRequest createCallout(){

		String bearerCredentials = 'c4XoQDprvLlEJ4uFHS9whzrcx:evtmiutkRQQ65keNkTdkldWVkmfRxuRiljAmiD306W2Gp3oxf2';
		String bearerCredentials64bit = EncodingUtil.base64Encode(Blob.valueof(bearerCredentials));

		system.debug('64BIT'+bearerCredentials64bit);
		HttpRequest requesttoTwitter = new HttpRequest();
		requesttoTwitter.setMethod('POST');
		//requesttoTwitter.setHeader('Authorization','Basic '+bearerCredentials64bit);
		requesttoTwitter.setBody('grant_type=client_credentials');
		requesttoTwitter.setEndpoint('https://api.twitter.com/oauth2/token');

		
		return requesttoTwitter;
	}

	public static HttpRequest createCallOutforSearch(String accessToken){
		system.debug('Access token'+accessToken);
		HttpRequest requesttoTwitterforSearch = new HttpRequest();
		requesttoTwitterforSearch.setMethod('GET');
		requesttoTwitterforSearch.setHeader('Authorization','Bearer '+accessToken);
		requesttoTwitterforSearch.setEndpoint('https://api.twitter.com/1.1/search/tweets.json?q=%23hardik&result_type=recent&count=5&include_entities=false');


		return requesttoTwitterforSearch;
	}

	public PageReference doCallOut(){

		Http client = new Http();
		allTexts = new List<wrappedResponse>();
		List<String> allIds = new List<String>();
		List<String> allText = new List<String>();
		List<String> userName = new List<String>();
		List<String> allUserIds = new List<String>();

		HttpResponse responsefromTwitter = client.send(TwitterHashTagCallOut.createCallout());
		lresponse = (loginResponse)JSON.deserialize(responsefromTwitter.getBody(),loginResponse.class);
		system.debug('responsefromTwitter'+lresponse);

		responseBody = responsefromTwitter.getBody();
		if(lresponse.access_token != null)
		{
			HTTP searchClient = new HTTP();
				HttpResponse responseforSearchFromTwitter = searchClient.send(TwitterHashTagCallOut.createCallOutforSearch(lresponse.access_token));
				responseBodyforSearch = responseforSearchFromTwitter.getBody();
				
				JSONParser parser = JSON.createParser(responseBodyforSearch);
		        while (parser.nextToken() != null) {
		            if ((parser.getText() == 'name') || (parser.getText() == 'id_str') 
		            	|| (parser.getText() == 'text')) {
		                // Get the value.
		                if(parser.getText() == 'id_str'){
		                	parser.nextToken();
		                	String id = parser.getText();
		                	if(id.startsWith('9'))
		                		allIds.add(parser.getText());
		                	else
		                		allUserIds.add(parser.getText());
		                }
		                else if(parser.getText() == 'text'){
		                	parser.nextToken();
		                	allText.add(parser.getText());		
		                }
		                else if(parser.getText() == 'name'){
		                	parser.nextToken();
		                	userName.add(parser.getText());
		                }
		                
		 			                
		            }
		        }

			system.debug('allIDs size'+allIds.size() + 'allTexts' + allText.size() + 'name' + userName.size() + 'allUserIds' + allUserIds.SIZE());

			for(Integer i=0;i<allIds.size();i++){
				wrappedResponse newResponse = new wrappedResponse();
				
				newResponse.link = 'https://twitter.com/statuses/'+allIds.get(i);
				if(userName.size() > i)
					newResponse.UserName = userName.get(i);
				if(allUserIds.size() > i)	
					newResponse.UserLink = 'https://twitter.com/intent/user?user_id='+allUserIds.get(i);
				allTexts.add(newResponse);
			}
			system.debug('allTexts'+allTexts);


		}
		return null;
	}
}